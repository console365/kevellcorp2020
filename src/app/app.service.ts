import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Photo} from './photo';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class DataService {
  private readonly API_URL = 'https://jsonplaceholder.typicode.com/photos';

  dataChange: BehaviorSubject<Photo[]> = new BehaviorSubject<Photo[]>([]);
  dialogData: any;
  toasterService: any;

  constructor(private httpClient: HttpClient) {}

  get data(): Photo[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllPhoto(): void {
    this.httpClient.get<Photo[]>(this.API_URL).subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
      });
  }

  addPhoto(photo: Photo): void {
    this.httpClient.post(this.API_URL, photo).subscribe(data => {
      this.dialogData = photo;
      alert('Successfully added');
      },
      (err: HttpErrorResponse) => {
      alert('Error occurred. Details: ' + err.name + ' ' + err.message);
    });
  }

  updatePhoto(photo: Photo): void {
    this.httpClient.put(this.API_URL + photo.id, photo).subscribe(data => {
      this.dialogData = photo;
      alert('Successfully edited');
    },
    (err: HttpErrorResponse) => {
      alert('Error occurred. Details: ' + err.name + ' ' + err.message);
    }
  );
  }

  deletePhoto(id: number): void {
    this.httpClient.delete(this.API_URL + id).subscribe(data => {
      console.log(data['']);
        alert('Successfully deleted');
      },
      (err: HttpErrorResponse) => {
        alert('Error occurred. Details: ' + err.name + ' ' + err.message);
      }
    );
  }
}

